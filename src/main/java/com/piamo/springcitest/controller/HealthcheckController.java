package com.piamo.springcitest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class HealthcheckController {

    @RequestMapping("/healthcheck")
    public ResponseEntity<?> getServiceStatus() {
        return ResponseEntity.ok(Map.of("status", "is alive"));
    }
}
