FROM openjdk:11-jre-slim-buster

WORKDIR /app

COPY target/spring-ci-test.jar  .

EXPOSE 8080

CMD ["java", "-jar", "spring-ci-test.jar"]